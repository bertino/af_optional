#include "solver.hpp"
#include "pprint.hpp"

Solver::Solver()
{
    this->nodes.emplace_back(Node());
    this->frontier.push(&(nodes.back()));
}

Solver::~Solver() { }

bool Solver::solve()
{
    pprint::PrettyPrinter printer;

    while (!frontier.empty()) {
        Node* current = frontier.top();
        std::vector<Node*> neighbors;

        std::pair<int, int> emptyTile = current->getEmptyTile();
        if (emptyTile.first > 0) {
            this->nodes.push_back(Node(current, Node::direction::up));
            neighbors.push_back(&this->nodes.back());
        }

        if (emptyTile.first < current->getSize() - 1) {
            this->nodes.emplace_back(Node(current, Node::direction::down));
            neighbors.push_back(&this->nodes.back());
        }

        if (emptyTile.second > 0) {
            this->nodes.emplace_back(Node(current, Node::direction::left));
            neighbors.push_back(&(this->nodes.back()));
        }

        if (emptyTile.second < current->getSize() - 1) {
            this->nodes.emplace_back(Node(current, Node::direction::right));
            neighbors.push_back(&(this->nodes.back()));
        }

        for (auto neighbor : neighbors) {

            if (neighbor->getEstimatedCost() == 0) {
                explored[neighbor->getKey()] = neighbor;
                return true;
            }

            if (explored.find(neighbor->getKey()) == explored.end() || neighbor < explored[neighbor->getKey()]) {
                frontier.push(neighbor);
            }
        }

        explored[current->getKey()] = current;
        frontier.pop();
    }

    return false;
}
