#ifndef NODE_H
#define NODE_H

#include "pprint.hpp"
#include <functional>
#include <iostream>
#include <vector>

typedef std::vector<std::vector<int>> matrix;

class Node {
public:
    enum direction { up, down, left, right };

private:
    Node* parent;
    direction action;
    int size;
    int currentCost, estimatedCost;
    size_t key;
    matrix state;
    std::pair<int, int> emptyTile;

    size_t calculateKey();
    void estimateCost();

public:
    friend std::ostream& operator<<(std::ostream& os, const Node& node);
    bool operator<(const Node& other) const;
    bool operator<=(const Node& other) const;
    bool operator>(const Node& other) const;
    bool operator>=(const Node& other) const;
    bool operator==(const Node& other) const;
    bool operator!=(const Node& other) const;
    Node* getParent() { return parent; }
    size_t getKey() { return key; }
    int getSize() { return size; }
    int getCost() { return currentCost; }
    int getEstimatedCost() { return estimatedCost; }
    int getTotalCost() { return currentCost + estimatedCost; }
    matrix getState() { return state; }
    std::pair<int, int> getEmptyTile() { return emptyTile; }
    Node();
    Node(Node* parent, direction action);
    virtual ~Node();
};

#endif /* NODE_H */
