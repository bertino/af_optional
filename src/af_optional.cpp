#include "solver.hpp"
#include <iostream>

int main()
{
    Solver solver;
    std::cout << solver.solve();
    std::cout << std::endl;
    return 0;
}
