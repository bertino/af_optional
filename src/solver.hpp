#ifndef SOLVER_H
#define SOLVER_H

#include "node.hpp"
#include <queue>
#include <unordered_map>

class Solver {
private:
    std::vector<Node> nodes;
    std::priority_queue<Node*> frontier;
    std::unordered_map<size_t, Node*> explored;
    
public:
    bool solve();

    Solver();
    virtual ~Solver();
};

#endif /* SOLVER_H */
