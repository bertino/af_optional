#include "node.hpp"

Node::~Node() { }

Node::Node()
{
    this->parent = nullptr;
    this->currentCost = 0;

    std::cin >> this->size;
    this->state = matrix(this->size);

    for (int i = 0; i < this->size; i++) {
        this->state[i] = std::vector<int>(this->size);
        for (int j = 0; j < this->size; j++) {
            std::cin >> this->state[i][j];
            if (state[i][j] == 9) this->emptyTile = std::make_pair(i, j);
        }
    }

    estimateCost();
    calculateKey();
}

Node::Node(Node* parent, direction action)
{
    this->parent = parent;
    this->size = parent->getSize();
    this->action = action;
    this->currentCost = parent->getCost() + 1;
    pprint::PrettyPrinter printer;
    this->state = parent->getState();
    int row = this->parent->getEmptyTile().first;
    int col = this->parent->getEmptyTile().second;

    int offsetX = 0, offsetY = 0;

    if (this->action == up) offsetY = -1;
    if (this->action == down) offsetY = 1;
    if (this->action == left) offsetX = -1;
    if (this->action == right) offsetX = 1;

    std::swap(this->state[row][col], this->state[row + offsetY][col + offsetX]);

    this->emptyTile = this->parent->getEmptyTile();

    this->emptyTile.first += offsetY;
    this->emptyTile.second += offsetX;

    calculateKey();
    estimateCost();
}

std::ostream& operator<<(std::ostream& os, const Node& node)
{
    pprint::PrettyPrinter printer(os);
    
    std::cout << "\nthis: " << &node << "\n";
    printer.print("parent: ", node.parent);
    printer.print("state:", node.state);
    printer.print("action:", node.action);
    printer.print("emptyTile: ", node.emptyTile);

    return os;
}

bool Node::operator<(const Node& other) const
{
    return this->currentCost + this->estimatedCost < other.currentCost + other.estimatedCost;
}

bool Node::operator<=(const Node& other) const
{
    return this->currentCost + this->estimatedCost <= other.currentCost + other.estimatedCost;
}

bool Node::operator>(const Node& other) const
{
    return this->currentCost + this->estimatedCost > other.currentCost + other.estimatedCost;
}

bool Node::operator>=(const Node& other) const
{
    return this->currentCost + this->estimatedCost >= other.currentCost + other.estimatedCost;
}

bool Node::operator==(const Node& other) const
{
    return this->currentCost + this->estimatedCost == other.currentCost + other.estimatedCost;
}

bool Node::operator!=(const Node& other) const
{
    return this->currentCost + this->estimatedCost != other.currentCost + other.estimatedCost;
}

size_t Node::calculateKey()
{
    size_t key = this->getSize();

    for (int i = 0; i < this->size; i++) {
        for (int j = 0; j < this->size; j++) {
            key ^= i + 0x9e3779b0 + (key << 6) + (key >> 2);
        }
    }

    return key;
}

void Node::estimateCost()
{
    this->estimatedCost = 0;
    for (int i = 0; i < this->size; i++) {
        for (int j = 0; j < this->size; j++) {
            int currentItem = this->state[i][j];
            int targetY = (currentItem - 1) / this->size;
            int targetX = (currentItem - 1) % this->size;
            this->estimatedCost += std::abs(i - targetY) + std::abs(j - targetX);
        }
    }
}
